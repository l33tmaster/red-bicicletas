var mongoose = require('mongoose'); 
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');


var base_url = "http://localhost:3000/api/bicicletas";

describe("Bicicleta API", () =>{
    beforeEach(function(done){
         
        var mongoDB = 'mongodb://localhost:27017/testdb';
        
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true});
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe('GET BICICLETAS /', ()=>{
        it("Status 200", (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                
            }); 
            done();       
        });
    });

    describe('POST BICICLETAS /create', () => {
        it("Status 200", (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 10, "color":"rojo", "modelo":"urbana", "lat": 19.3746482,"lng":-99.1811512}';

            request.post({
                headers:    headers,
                url:        base_url + '/create',
                body:       aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(19.3746482);
                expect(bici.ubicacion[1]).toBe(-99.1811512);

                
            });
        
        done();
        });
    });
    /*
    describe('POST BICICLETAS /delete', () => {
        it("Status 204", (done) =>{
            var a = Bicicleta.createInstance(1, "negro", "urbana", [19.3746482,-99.1811512]);
            Bicicleta.add(a. function (err, newBici) {
                var headers={'content-type': 'application/json'};
            )
            })
        })
    })*/

});

/*
describe('Bicicleta API', () => {

    describe('GET bicicletas /', () => {
        it('Status 200', ()=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, 'rojo', 'urbana', [19.3746482,-99.1811512])
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas'), function(error, response, body){
                expect(response.statusCode).toBe(200);
            }

        });
    
    });

    describe('POST bicicletas /create', () => {
        it('Status 200', (done)=>{
            var headers = {'content-type':'application/json'};
            var a = '{"id": 1, "color":"rojo", "modelo":"urbana", "lat": 19.3746482,"lng":-99.1811512}';

            request.post({
                headers:    headers,
                url:        'http://localhost:3000/api/bicicletas/create',
                body:       a
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(1).color).toBe("rojo");
                    done();
            });

        });
    
    });

});
*/