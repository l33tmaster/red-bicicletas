var mongoose = require('mongoose'); 
var Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva');
var Usuario = require('../../models/usuario');

describe('Testing Usuario', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true});
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    mongoose.disconnect(err); 
                    done();
                }); 
            });
    
        });
    });

    describe('Cuando un Usuario reserva una bici', () =>{
        it('debe exisitir la reserva', (done) =>{
            const usuario = new Usuario({nombre:'Ruben'});
            usuario.save();
            const bicicleta = new Bicicleta({color:'verde', modelo:"urbana"});
            bicicleta.save();
    
            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    //expect(reservas[0].bicicleta.code).toBe(10);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });           
            });
            
        });
    
    });

});
