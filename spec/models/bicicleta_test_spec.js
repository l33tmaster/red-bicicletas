var mongoose = require('mongoose'); 
var Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost:27017/test_db';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true});
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var a = Bicicleta.createInstance(1,"rojo", "urbana", [-99.1811512,19.3746482]);

            expect(a.code).toBe(1);
            expect(a.color).toBe("rojo");
            expect(a.modelo).toBe("urbana");
            expect(a.ubicacion[1]).toEqual(19.3746482);
            expect(a.ubicacion[0]).toEqual(-99.1811512);
                       
        });
        
    });

    describe("Bicicleta.allBicis", () => {
        it("comienza vacía", (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toEqual(0);
                done();
        
            });
        });    
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', function(done) {
            var aBici = new Bicicleta({code:1, color:"verde", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    
                    
                });
            });
        done();
        });
    });

    describe('Bicicleta.findByCode', () =>{
        it('debe devolver la bici con code 1', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1, color:"verde", modelo: "urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code:2, color:"rojo", modelo: "urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toBe(1);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            
                        });
                    });
                });

                done();
            });
        });
    });
});

/*
beforeEach(() => {Bicicleta.allBicis = []; });

describe(Bicicleta.allBicis, () => {
    it('empieza vacia', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });

});

describe(Bicicleta.add, () => {
    it('agregar nueva', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [19.3746482,-99.1811512])

        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });

});

describe(Bicicleta.findById, () => {
    it('encontrar por id', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [19.3746482,-99.1811512])
        var b = new Bicicleta(2, 'blanco', 'urbana', [19.3884535,-99.1769126])

        Bicicleta.add(a);
        Bicicleta.add(b);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);
    });

});
*/