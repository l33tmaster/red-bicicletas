const Bicicleta = require('../models/bicicleta');

module.exports = {    
    list: function(req, res, next){
        Bicicleta.find({}, (err, bicicleta) => {
            res.render('bicicletas/index', {bicicletas:bicicleta});
        });
},
  create_get : function(req, res, next){
    res.render('bicicletas/create', {errors:{}, bici: new Bicicleta()});

    
},

 create_post : function(req, res){
    /*var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);*/
    Bicicleta.create({color: req.body.color, modelo: req.body.modelo, ubicacion:[req.body.lng, req.body.lat]}, function(err, nuevaBici){
        
        res.redirect('/bicicletas');
    },

    )},

    update_get: function(req, res, next){
        Bicicleta.findById(req.params.id, function(err, bicicleta){
            res.render('bicicletas/update', {errors:{}, bici: bicicleta});
        });
    },

 update_post : function(req, res, next){
    var update_values = {color : req.body.color,
     modelo : req.body.modelo,
        ubicacion : [req.body.lat, req.body.lng],
    };
    
    Bicicleta.findByIdAndUpdate(req.params.id, update_values,function (err, bicicleta){
        if(err){
            console.log(err);
            res.render('bicicletas/update' , {
                errors: err.errors,
                //usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})
            });
        }else{
            res.redirect('/bicicletas');
            return;
        }
        ;
        });
    /*bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];*/

    
},

    delete_post : function(req, res, next){
        Bicicleta.findByIdAndDelete(req.body.id, function(err){
            if (err)
                next(err);
            else 
                res.redirect('/bicicletas');
        });
},
}