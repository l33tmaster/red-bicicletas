var Bicicleta = require('../../models/bicicleta');

/*
exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}
 */
exports.bicicleta_list = function (req, res) {

    Bicicleta.allBicis((error, bicis) => {
    
    if (error) return res.status(500).send({ message: `Error al realizar la peticion: ${error}` });
    
    if (!bicis) return res.status(404).send({ message: 'NO existen bicicletas' });
    
    res.status(200).json({ bicicletas: bicis });
    
    });
    
}

exports.bicicleta_create = function(req, res){
    /*
    var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    */
    const bici = new Bicicleta();
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;

    // Pasar primero la longitud y luego la latitud por defecto!
    bici.ubicacion = [req.body.lng, req.body.lat];

    /*
    Bicicleta.(bici);

    res.status(200).json({
        Bicicleta: bici
    });
    */

    bici.save(function(err){
        res.status(200).json(bici);
    });

}

exports.bicicleta_delete = function(req, res){
    
    Bicicleta.removeById(req.body.code);

    res.status(204).send();

}